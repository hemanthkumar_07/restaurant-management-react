import React from 'react';
import './App.css';
import Register from './components/Registerpage';
import Update from './components/updatepage';
import InfoData from './components/RestaurantInfoPage';
import Delete from './components/deletepage';
import Landing from './components/landingpage';
import AllRestaurantInfo from './components/RestaurantInfoPage/profileIndex';
import GetAlldata from './components/AllRestaurantInfo';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import RestaurantInfo from './components/AllRestaurantInfo/info';
import UpadateCode from './components/sucesspage/updatecode';
import RegisterCode from './components/sucesspage/registercode';
import DeleteCode from './components/sucesspage/deletecode';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/' exact component={Landing} />
        <Route path='/register' exact component={Register} />
        <Route path='/registercode' exact component={RegisterCode} />
        <Route path='/updatepage' exact component={Update} />
        <Route path='/updatecode' exact component={UpadateCode} />
        <Route path='/deletepage' exact component={Delete} />
        <Route path='/deletecode' exact component={DeleteCode} />
        <Route path='/RestaurantInfoPage' exact component={InfoData} />
        <Route path="/RestaurantDetails" exact component={AllRestaurantInfo} />
        <Route path="/data" exact component={GetAlldata} />
        <Route path="/resdata" exact component={RestaurantInfo} />
      </Switch>
    </BrowserRouter>

  );
}

export default App;
