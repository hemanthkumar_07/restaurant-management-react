
import React from "react";
import "../RestaurantInfoPage/index.css";
import {
    Table,
    TableHead,
    TableCell,
    TableRow,
    TableBody,

    makeStyles,
} from "@material-ui/core";
import { Link } from "react-router-dom";
const useStyles = makeStyles({
    table: {
        width: "100%",
        margin: "5px 0 0 5px",
    },
    thead: {
        "& > *": {
            fontSize: 15,
            background: "",
            color: "red",
        },
    },
    row: {
        "& > *": {
            fontSize: 13,
        },

    },
    background: {
        overflowX: "auto"
    }
});
function RestaurantInfo() {
    const classes = useStyles();
    const data = localStorage.getItem("restaurantData")
    const rdata = JSON.parse(data)
    console.log(rdata)
    return (
        <div>
            <header>
                <div className="container">
                    <nav className="pre-nav">
                        <a href="/#" className="logo"><i className="fas fa-utensils" /><span style={{ color: "orange" }}>Rest</span>aurant Management</a>
                        <a href="/#">Call us on +91 8008701187</a>
                    </nav>
                    <nav>
                        <ul>

                            <li><Link to='/register'><a href="/#" ><u>Register Data</u></a></Link></li>
                            <li><Link to='/updatepage'><a href="/#"><u>Update Data</u></a></Link></li>
                            <li><Link to='/deletepage'><a href="/#"><u>Delete Data</u></a></Link></li>
                            <li><Link to='/RestaurantInfoPage'><a href="/#"><u>Get Data</u></a></Link></li>
                            <li><Link to='/RestaurantDetails'><a href="/#"><u>Get All Data</u></a></Link></li>
                        </ul>
                    </nav>

                    <div className={classes.background}>
                        <Table className={classes.table}>
                            <TableHead>
                                <TableRow className={classes.thead}>
                                    <TableCell>Restaurant Name</TableCell>
                                    <TableCell>Manager Name</TableCell>
                                    <TableCell>Contact No</TableCell>
                                    <TableCell>Menu</TableCell>
                                    <TableCell>Timings</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <TableRow className={classes.row}>
                                    <TableCell>{rdata.data.restaurant_name}</TableCell>
                                    <TableCell>{rdata.data.manager_name}</TableCell>
                                    <TableCell>{rdata.data.contact_no}</TableCell>
                                    <TableCell>{rdata.data.menu}</TableCell>
                                    <TableCell>{rdata.data.timings}</TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>

                    </div>
                </div>
            </header>

        </div>
    );
}

export default RestaurantInfo;
