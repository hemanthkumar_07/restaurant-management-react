
import React from "react";
// import CountrySelector from "../countrycode.js";
import { Link } from "react-router-dom";
import '../Registerpage/index.css';
import {
  _contact_no_checker,
  _Restaurant_managerName_checker,
  _Restaurant_Name_checker,
  _Menu_checker,
  _timings_checker,
  _return_object_keys,
} from "../../validators/helperfunction.js";
class Register extends React.Component {

  state = {
    restaurant_name: "",
    restaurant_name_err: "",
    manager_name: "",
    manager_name_err: "",
    contact_no: "",
    contact_no_err: "",
    menu: "",
    menu_err: "",
    timings: "",
    timings_err: "",
    // contact_no_db_err:""
  }
  ReisterSucess = () => {
    const { history } = this.props
    history.push('/registercode')
  }
  // apiCallFail = (data) => {
  //   this.setState({ isSignUp: true, error_msg: data.status_message })
  apiCallFail = (data) => {
    this.setState({ isSignUp: true, error_msg: data.status_message })

  }
  RegisterApiCall = async (event) => {
    event.preventDefault();
    console.log(this.state);

    const { restaurant_name, manager_name, contact_no, menu, timings } = this.state
    const url = "http://localhost:3001/register"
    const RegisterData = {
      restaurant_name,
      manager_name,
      contact_no,
      menu,
      timings

    }
    const option = {
      method: "POST",
      body: JSON.stringify(RegisterData),
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      }
    }
    const response = await fetch(url, option)
    const data = await response.json()
    console.log(data);
    if (data.statuscode === 200) {
      this.ReisterSucess()
    }
    else {
      this.apiCallFail(data)
    }
  }
  changeRestaurantName = (event) => {
    this.setState({ restaurant_name: event.target.value, restaurant_name_err: "" })
  }
  validateRestaurantName = () => {
    const restaurant_name = this.state.restaurant_name;
    const restaurant_name_errors = _Restaurant_Name_checker(restaurant_name);
    const is_restaurant_name_validated =
      _return_object_keys(restaurant_name_errors).length === 0;
    console.log(restaurant_name_errors);
    console.log(is_restaurant_name_validated);
    if (!is_restaurant_name_validated) {
      // product_name validation failed
      this.setState({ restaurant_name_err: restaurant_name_errors.restaurant_name });
    }
  };

  changeManagerName = (event) => {
    this.setState({ manager_name: event.target.value, manager_name_err: "" })
  }
  validateManagerName = () => {
    const manager_name = this.state.manager_name;
    const manager_name_errors = _Restaurant_managerName_checker(manager_name);
    const is_manager_name_validated =
      _return_object_keys(manager_name_errors).length === 0;
    console.log(manager_name_errors);
    console.log(is_manager_name_validated);
    if (!is_manager_name_validated) {
      // product_name validation failed
      this.setState({ manager_name_err: manager_name_errors.manager_name });
    }
  };

  changeContactNo = (event) => {
    this.setState({ contact_no: event.target.value, contact_no_err: "" })
  }
  validateContactNo = () => {
    const contact_no = this.state.contact_no;
    const contact_no_errors = _contact_no_checker(contact_no);
    const is_contact_no_validated =
      _return_object_keys(contact_no_errors).length === 0;
    console.log(contact_no_errors);
    console.log(is_contact_no_validated);
    if (!is_contact_no_validated) {
      // product_name validation failed
      this.setState({ contact_no_err: contact_no_errors.contact_no });
    }
  };
  changeMenu = (event) => {
    this.setState({ menu: event.target.value, menu_err: "" })
  }
  validateMenu = () => {
    const menu = this.state.menu;
    const menu_errors = _Menu_checker(menu);
    const is_menu_validated =
      _return_object_keys(menu_errors).length === 0;
    console.log(menu_errors);
    console.log(is_menu_validated);
    if (!is_menu_validated) {
      // product_name validation failed
      this.setState({ menu_err: menu_errors.menu });
    }
  };
  changeTimings = (event) => {
    this.setState({ timings: event.target.value, timings_err: "" })
  }
  validateTimings = () => {
    const timings = this.state.timings;
    const timings_errors = _timings_checker(timings);
    const is_timings_validated =
      _return_object_keys(timings_errors).length === 0;
    console.log(timings_errors);
    console.log(is_timings_validated);
    if (!is_timings_validated) {
      this.setState({ timings_err: timings_errors.timings });
    }
  };
  render() {

    return (

      
        <header>
          <div className="container">
            <nav className="pre-nav">
              <a href="/#" className="logo"><i className="fas fa-utensils" /><span style={{ color: "orange" }}>Rest</span>aurant Management</a>
              <a href="/#">Call us on +91 8008701187</a>
            </nav>
            <nav>
              <ul>

                <li><Link to='/register'><a href="/#" ><u>Register Data</u></a></Link></li>
                <li><Link to='/updatepage'><a href="/#"><u>Update Data</u></a></Link></li>
                <li><Link to='/deletepage'><a href="/#"><u>Delete Data</u></a></Link></li>
                <li><Link to='/RestaurantInfoPage'><a href="/#"><u>Get Data</u></a></Link></li>
                <li><Link to='/RestaurantDetails'><a href="/#"><u>Get All Data</u></a></Link></li>
               
              </ul>
            </nav>
            <div className="wrapper">
              <div id="formContent">
                <div className="container">
                  <div id="bd" className>
                    <div id="Content">
                      <div className="form-fields-section">
                        <h1>Register Data</h1>
                        <div className="registration">
                          <div className="bg-form">
                            <h3>Please enter your information</h3>
                            <form method="post" action="/app/register" id="registration-form" onSubmit={this.RegisterApiCall} >
                              <fieldset>
                                <ul>
                                  <li className="field">
                                    <label For="restaurant name"></label>
                                    <div className="field__wrapper">
                                      <input size={40} maxLength={35} name="restaurantName" placeholder="Restaurant Name" id="restaurant name" type="text" className="field__input field__input--pw" required onChange={this.changeRestaurantName} onBlur={this.validateRestaurantName} /></div>
                                    <p style={{ color: "red" }}>{this.state.restaurant_name_err}</p>
                                  </li>
                                  <li className="field">
                                    <label For="manager name"></label>
                                    <div className="field__wrapper">
                                      <input size={40} maxLength={50} id="managername" placeholder="Manager Name" type="text" className="field__input field__input--pw" required onChange={this.changeManagerName} onBlur={this.validateManagerName} /></div>
                                    <p style={{ color: "red" }}>{this.state.manager_name_err}</p>
                                  </li>
                                  <li className="field">
                                    <label For="contact"></label>
                                    <div className="field__wrapper">
                                      <input size={25} maxLength={35} id="contact" placeholder="contact No" type="text" className="field__input field__input--pw" required onChange={this.changeContactNo} onBlur={this.validateContactNo} /></div>
                                    <p style={{ color: "red" }}>{this.state.contact_no_err}</p>
                                  </li>
                                  <li className="field">
                                    <label For="menu"></label>
                                    <div className="field__wrapper">
                                      <input size={25} maxLength={35} id="menu" placeholder="Menu" type="text" className="field__input field__input--pw" required onChange={this.changeMenu} onBlur={this.validateMenu} /></div>
                                    <p style={{ color: "red" }}>{this.state.menu_err}</p>
                                  </li>
                                  <li className="field">
                                    <label For="timings" ></label>
                                    <div className="field__wrapper">
                                      <input size={40} maxLength={50} id="timings" placeholder="Timings" type="text" className="field__input field__input--pw" required onChange={this.changeTimings} onBlur={this.validateTimings} /></div>
                                    <p style={{ color: "red" }}>{this.state.timings_err}</p>
                                  </li>
                                </ul>

                                <button >
                                  <input className="btn btn-primary" value="Submit" /></button>

                              </fieldset>

                            </form>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </header >
    
    )
  }
}
export default Register; 