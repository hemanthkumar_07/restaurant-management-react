import React from "react";
import { Link } from "react-router-dom";

import '../RestaurantInfoPage/index.css';

class InfoData extends React.Component {
	state = {
		restaurant_name: " ",
	};

	getSuccess = () => {
		const { history } = this.props
		history.push("/resdata");
	};

	apiCallFail = (data) => {
		this.setState({ error_msg: data.err_msg });
	}

	getApiCall = async (event) => {
		event.preventDefault();
		console.log(this.state)
		const { restaurant_name } = this.state;
		const url = "http://localhost:3001/about_restaurant";
		const userDetails = {
			restaurant_name,
		}
		const option = {
			method: "POST",
			body: JSON.stringify(userDetails),
			headers: {
				"Content-Type": "application/json",
				Accept: "application/json",
			},
		};
		const response = await fetch(url, option);
		const data = await response.json();
		console.log("backend data", data);
		console.log(data);
		if (data.statuscode === 200) {
			console.log(data)
			localStorage.setItem("restaurantData", JSON.stringify(data))
			this.getSuccess()
		} else {
			this.apiCallFail(data)
		}
	};
	changeRestaurantDetails = (event) => {
		this.setState({ restaurant_name: event.target.value })
	}
	render() {
		return (
			<div>
				<header>


					<div className="container">
						<nav className="pre-nav">
							<a href="/#" className="logo"><i className="fas fa-utensils" /><span style={{ color: "orange" }}>Rest</span>aurant Management</a>
							<a href="/#">Call us on +91 8008701187</a>
						</nav>
						<nav>
							<ul>
								<li><Link to='/register'><a href="/#" ><u>Register Data</u></a></Link></li>
								<li><Link to='/updatepage'><a href="/#"><u>Update Data</u></a></Link></li>
								<li><Link to='/deletepage'><a href="/#"><u>Delete Data</u></a></Link></li>
								<li><Link to='/RestaurantInfoPage'><a href="/#"><u>Get Data</u></a></Link></li>
								<li><Link to='/RestaurantDetails'><a href="/#"><u>Get All Data</u></a></Link></li>
							</ul>

						</nav>

						<div>
							<div className="wrapper">
								<div id="formContent">
									<div className="wrapper">
										<form className="form-signin" onSubmit={this.getApiCall}>
											<h2 className="form-signin-heading">Restaurant Data</h2>
											<input type="text" className="form-control" name="username" placeholder="Restaurant Name" required onChange={this.changeRestaurantDetails} /><br />
											{/* <button className="btn" type="submit" style={{ backgroundColor: "slateblue" }}>Get Data</button> */}
											<input type="submit" className="fadeIn fourth" defaultValue="update" style={{ color: "red" }} />
										</form>
									</div>
								</div>
							</div>
						</div>

					</div>
				</header>

			</div>
		);
	}
}
export default InfoData;