
import React, { useEffect, useState } from "react";
import "../RestaurantInfoPage/index.css";

import {
    Table,
    TableHead,
    TableCell,
    TableRow,
    TableBody,

    makeStyles,
    TableContainer,
} from "@material-ui/core";
import { Link } from "react-router-dom";
// import { Link } from "react-router-dom";
const useStyles = makeStyles({
    table: {
        width: "100%",
        margin: "5px 0 0 5px",
    },
    thead: {
        "& > *": {
            fontSize: 15,
            background: "",
            color: "red",
        },
    },
    row: {
        "& > *": {
            fontSize: 13,

        },

    },
    background: {
        overflowX: "auto"
    }
});
function AllRestaurantInfo() {
    const [restaurantdata, setrestaurantdata] = useState([])
    const classes = useStyles();
    useEffect(() => {
        async function fetchData() {
            const option = {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
            };
            const response = await fetch("http://localhost:3001/getalldetails", option)
            const data = await response.json()
            console.log(data);
            setrestaurantdata(data)
        }
        fetchData();
    }, [])
    return (
        <div>

            <header>


                <div className="container">
                    <nav className="pre-nav">
                        <a href="/#" className="logo"><i className="fas fa-utensils" /><span style={{ color: "orange" }}>Rest</span>aurant Management</a>
                        <a href="/#">Call us on +91 8008701187</a>
                    </nav>
                    <nav>
                        <ul>
                            <li><Link to='/register'><a href="/#" ><u>Register Data</u></a></Link></li>
                            <li><Link to='/updatepage'><a href="/#"><u>Update Data</u></a></Link></li>
                            <li><Link to='/deletepage'><a href="/#"><u>Delete Data</u></a></Link></li>
                            <li><Link to='/RestaurantInfoPage'><a href="/#"><u>Get Data</u></a></Link></li>
                            <li><Link to='/RestaurantDetails'><a href="/#"><u>Get All Data</u></a></Link></li>
                        </ul>
                    </nav>

                    <div className={classes.background}>
                        <TableContainer className={classes.root}>
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow className={classes.thead}>
                                        <TableCell>Restaurant Name</TableCell>
                                        <TableCell>Manager Name</TableCell>
                                        <TableCell>Contact No</TableCell>
                                        <TableCell>Menu</TableCell>
                                        <TableCell>Timings</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody style={{ background: "#026e78", opacity: "0.6" }}>
                                    {restaurantdata.map((Details) => (
                                        <TableRow className={classes.row} key={Details.id}>
                                            <TableCell>{Details.restaurant_name}</TableCell>
                                            <TableCell>{Details.manager_name}</TableCell>
                                            <TableCell>{Details.contact_no}</TableCell>
                                            <TableCell>{Details.menu}</TableCell>
                                            <TableCell>{Details.timings}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <br></br>
                        {/* <Button
                color="primary"
                variant="contained"
                style={{ marginRight: 10 }}
                component={Link}
                to={`/`}
            >
                Edit
            </Button> */}

                        {/* <Button
                color="primary"
                variant="contained"
                style={{ marginRight: 10 }}
                component={Link}
                to={`/`}
            >
                Delete
            </Button> */}
                        {/* <Button
                color="primary"
                variant="contained"
                style={{ marginRight: 10 }}
                component={Link}
                to={`/`}
            >
                Back to Home
            </Button> */}

                    </div>

                </div>
            </header>

        </div>
    );
}

export default AllRestaurantInfo;
