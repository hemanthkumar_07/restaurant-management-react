import React from "react";
import { Link } from "react-router-dom";

import '../deletepage/index.css';


class Delete extends React.Component {
	state = {
		restaurant_name: "",
	}

	signUpSuccess = () => {
		const { history } = this.props;
		history.push("/deletecode");
	};

	apiCallFail = (data) => {
		this.setState({ isSignUp: true, error_msg: data.status_message });
	};

	submitApiCall = async (event) => {
		event.preventDefault();
		console.log(this.state);

		const { restaurant_name } = this.state;
		const url = "http://localhost:3001/delete_data";
		const userDetails = {
			restaurant_name,

		}
		console.log('checking');
		const option = {
			method: "DELETE",
			body: JSON.stringify(userDetails),
			headers: {
				"Content-Type": "application/json",
				Accept: "application/json",
			},
		}
		const response = await fetch(url, option);
		const data = await response.json();
		console.log(data);
		if (data.statuscode === 200) {
			this.signUpSuccess();
		} else {
			this.apiCallFail(data);
		}
	}

	changeRestaurantName = (event) => {
		this.setState({ restaurant_name: event.target.value });
	}
	render() {
		return (
			
				<header>
					

					<div className="container">
						<nav className="pre-nav">
							<a href="/#" className="logo"><i className="fas fa-utensils" /><span style={{ color: "orange" }}>Rest</span>aurant Management</a>
							<a href="/#">Call us on +91 8008701187</a>
						</nav>
						<nav>
							<ul>
								<li><Link to='/register'><a href="/#" ><u>Register Data</u></a></Link></li>
								<li><Link to='/updatepage'><a href="/#"><u>Update Data</u></a></Link></li>
								<li><Link to='/deletepage'><a href="/#"><u>Delete Data</u></a></Link></li>
								<li><Link to='/RestaurantInfoPage'><a href="/#"><u>Get Data</u></a></Link></li>
								<li><Link to='/RestaurantDetails'><a href="/#"><u>Get All Data</u></a></Link></li>
							</ul>
						</nav>
						<div className="wrapper">
							<div id="formContent">
								<div className="wrapper">
									<form className="form-signin" onSubmit={this.submitApiCall}>
										<h2 className="form-signin-heading">Restaurant Data</h2>
										<input type="text" className="form-control" name="username" placeholder="Restaurant Name" required onChange={this.changeRestaurantName} /><br />

										{/* <input type="button" className="button" value="Delete Data" /> */}
										<input type="submit" className="fadeIn fourth" defaultValue="update" style={{ color: "red" }} />

									</form>
								</div>
							</div>
						</div>


					</div>
				</header>



		


		);
	}
}
export default Delete;