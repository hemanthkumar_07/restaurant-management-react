import React from "react";
import { Link } from "react-router-dom";
import '../landingpage/index.css';

class Landing extends React.Component {
	render() {
		return (
			<header>
				<div className="container">
					<nav className="pre-nav">
						<a href="/#" className="logo"><i className="fas fa-utensils" /><span style={{ color: "orange" }}>Rest</span>aurant Management</a>
						<a href="/#">Call us on +91 8008701187</a>
					</nav>
					<nav>
						<ul>
							<li><Link to='/register'><a href="/#" ><u>Register Data</u></a></Link></li>
							<li><Link to='/updatepage'><a href="/#"><u>Update Data</u></a></Link></li>
							<li><Link to='/deletepage'><a href="/#"><u>Delete Data</u></a></Link></li>
							<li><Link to='/RestaurantInfoPage'><a href="/#"><u>Get Data</u></a></Link></li>
							<li><Link to='/RestaurantDetails'><a href="/#"><u>Get All Data</u></a></Link></li>
						</ul>
					</nav>
					<div className="content">
						<h1>Welcome</h1>
						<a href="/#" className="white-link">Health is Wealth</a>
						<a href="/#" className="teal-link">View Our Menu</a>
					</div>
				</div>
			</header>
		)

	}
}
export default Landing;