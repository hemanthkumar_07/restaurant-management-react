import React from "react";
import '../updatepage/index.css';
import { Link } from "react-router-dom";

class Update extends React.Component {
	state = {
		restaurant_name: " ",
		new_menu: " ",
		new_timings: 0,

	}
	submitSuccess = () => {
		const { history } = this.props
		history.push("/updatecode")
	}
	apiCallFail = (data) => {
		this.setState({ isSignUp: true, error_msg: data.status_message })
	}
	submitApiCall = async (event) => {
		event.preventDefault();
		console.log(this.state);
		const { restaurant_name, new_menu, new_timings } = this.state
		const url = "http://localhost:3001/update"
		const RestaurantDetails = {
			restaurant_name,
			new_menu,
			new_timings

		}
		const option = {
			method: "PUT",
			body: JSON.stringify(RestaurantDetails),
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json"
			}
		}
		const response = await fetch(url, option)
		const data = await response.json()
		console.log(data);
		if (data.statuscode === 200) {
			this.submitSuccess()
		}
		else {
			this.apiCallFail(data)
		}
	}
	ChangeRestaurantName = (event) => {
		this.setState({ restaurant_name: event.target.value })

	}

	ChangeNewMenu = (event) => {
		this.setState({ new_menu: event.target.value })
	}

	ChangeNewTimings = (event) => {
		this.setState({ new_timings: event.target.value })
	}

	render() {
		return (

			<div>

				<header>


					<div className="container">
						<nav className="pre-nav">
							<a href="/#" className="logo"><i className="fas fa-utensils" /><span style={{ color: "orange" }}>Rest</span>aurant Management</a>
							<a href="/#">Call us on +91 8008701187</a>
						</nav>
						<nav>
						<ul>
							<li><Link to='/register'><a href="/#" ><u>Register Data</u></a></Link></li>
							<li><Link to='/updatepage'><a href="/#"><u>Update Data</u></a></Link></li>
							<li><Link to='/deletepage'><a href="/#"><u>Delete Data</u></a></Link></li>
							<li><Link to='/RestaurantInfoPage'><a href="/#"><u>Get Data</u></a></Link></li>
							<li><Link to='/RestaurantDetails'><a href="/#"><u>Get All Data</u></a></Link></li>
						</ul>
						</nav>
						<div className="wrapper">
							<div id="formContent">
								{/* <h2 className="active"> Update Data </h2> */}
								<h2> Update Details </h2>
								<form onSubmit={this.submitApiCall}>
									<input type="text" id="login" className="fadeIn second" placeholder="restaurant name" required onChange={this.ChangeRestaurantName} />
									<input type="text" id="password" className="fadeIn third" placeholder="new menu" required onChange={this.ChangeNewMenu} />
									<input type="text" id="password" className="fadeIn third" placeholder="new timings" required onChange={this.ChangeNewTimings} />
									<input type="submit" className="fadeIn fourth" defaultValue="update" style={{ color: "red" }} />
								</form>

								<div id="formFooter">
									<a className="underlineHover" href="/#" style={{color:"#026e78"}}>Reastaurant Managament</a>
								</div>
							</div>
						</div>


					</div>
				</header>

				{/* <div className="wrapper fadeInDown">
					<div id="formContent">
						<h2 className="active"> Update Data </h2>
						<form onSubmit={this.submitApiCall}>
							<input type="text" id="login" className="fadeIn second" placeholder="restaurant name" required onChange={this.ChangeRestaurantName} />
							<input type="text" id="password" className="fadeIn third" placeholder="new menu" required onChange={this.ChangeNewMenu} />
							<input type="text" id="password" className="fadeIn third" placeholder="new timings" required onChange={this.ChangeNewTimings} />
							<input type="submit" className="fadeIn fourth" defaultValue="update" style={{ color: "red" }} />
						</form>

						<div id="formFooter">
							<a className="underlineHover" href="/#">Reastaurant Managament</a>
						</div>
					</div>
				</div> */}

			</div>

		);
	}
}
export default Update;