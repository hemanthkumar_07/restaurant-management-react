

/**
 * This is a helper function to check and validate the emp_id
 * @param {} restaurant_name this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _Restaurant_Name_checker(restaurant_name) {
  const returnObject = {};

  // check if its not sent
  if (restaurant_name === undefined) {
    returnObject.restaurant_name = "Parameter restaurant_name is not sent";
    return returnObject;
  }
  if(!restaurant_name.match(/^[a-zA-Z ]*$/)){
    returnObject.restaurant_name = "restaurant name shounld be only alphabets";
    return returnObject;
  }
  // check if it is numeric type
  if (typeof restaurant_name !== "string") {
    returnObject.restaurant_name = 'Parameter "' + restaurant_name + '" is not a string type';
    return returnObject;
  }
  if(restaurant_name.length === 0){
    returnObject.restaurant_name = "restaurant name should be mandatory";
    return returnObject;
  }


  return returnObject;
}



/**
 * This is a helper function to check and validate the password
 * @param {} manager_name this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _Restaurant_managerName_checker(manager_name) {
  const returnObject = {};

  // check if its not sent
  if (manager_name === undefined) {
    returnObject.manager_name = "Parameter manager name is not sent";
    return returnObject;
  }
  if(!manager_name.match(/^[a-zA-Z]*$/)){
    returnObject.manager_name = "manager name shounld be only alphabets";
    return returnObject;
  }
  // check if it is string type
  if (typeof manager_name !== "string") {
    returnObject.manager_name = "manager name must be a string";
    return returnObject;
  }
  if(manager_name.length === 0){
    returnObject.manager_name = "manager name should be mandatory";
    return returnObject;
  }
  return returnObject;
}

/**
 * This is a helper function to check and validate the menu
 * @param {} menu this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _Menu_checker(menu) {
  const returnObject = {};
  if (menu === undefined) {
    returnObject.menu = "menu not given";
    return returnObject;
  }
  if(menu.length === 0){
    returnObject.menu = "menu should be mandatory";
    return returnObject;
  }
  // if(!menu.match(/^[a-zA-Z ]*$/)){
  //   returnObject.menu = "menu shounld be only alphabets";
  //   return returnObject;
  // }
  if (typeof menu !== "string") {
    returnObject.menu = "menu must be a string";
    return returnObject;
  }
  if (menu.length <= 8 && menu.length > 6) {
    returnObject.menu =
      "menu length must be in between 6 to 8 characters";
  }
}

/**
 * This is a helper function to check and validate the timings
 * @param {} timings this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _timings_checker(timings) {
  const returnObject = {};
  
  if (timings === undefined) {
    returnObject.timings = "timings not given";
    return returnObject;
  }
  if (typeof timings !== "string") {
    returnObject.timings = "timings must be a string";
    return returnObject;
  }
  if(timings.length === 0){
    returnObject.timings = "timings should be mandatory";
    return returnObject;
  }

  return returnObject;
}



/**
 * This is a helper function to check and validate the contact_no
 * @param {} contact_no this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _contact_no_checker(contact_no) {
  const returnObject = {};
  if (contact_no === undefined) {
    returnObject.contact_no = "contact No Number Not given";
    return returnObject;
  }
  
  if(contact_no.toString().length === 0){
    returnObject.contact_no = "contact no mandatory";
    return returnObject;
  }
 
  if (contact_no.toString().length !== 10) {
    returnObject.contact_no = "Mobile number should have 10 digits: " + contact_no;
    return returnObject;
  }
  if (isNaN(contact_no)) {
    returnObject.contact_no = "contact Number should be a Number type";
    return returnObject;
  }

  return returnObject;
}


/**
 * This is a helper function to count the keys in object recieved
 */
function _return_object_keys(obj) {

  let returnArray = [];
  for (let key in obj) {
    returnArray.push(key);
  }

  return returnArray;
}


export {
  _Restaurant_Name_checker,
  _contact_no_checker,
  _Restaurant_managerName_checker,
  _Menu_checker,
  _timings_checker,
  _return_object_keys
};
